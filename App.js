import React from 'react';
import {StyleSheet, View} from 'react-native';
import Routes from './src/Routes';

import {createStore} from 'redux';
import {Provider} from 'react-redux';

const initialState = {
  email: '',
  password: '',
  user: [
    {
      id: 'test@sample.com',
      password: 'test123',
    },
  ],
  titleArray: [{key: 'sample'}],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_USER_EMAIL':
      return {
        ...state,
        email: action.email,
      };
    case 'SET_USER_PASSWORD':
      return {
        ...state,
        password: action.password,
      };
    case 'SET_USER_DATA':
      return {
        ...state,
        user: state.user.concat(action.user),
      };
    case 'SET_TITLE':
      return {
        ...state,
        titleArray: state.titleArray.concat({key: action.title}),
      };
    case 'DELETE_TITLE':
      return {
        ...state,
        titleArray: state.titleArray.filter(el => el.key !== action.title),
      };
  }
  return state;
};

const store = createStore(reducer);

const App = () => {
  return (
    <Provider store={store}>
      <View style={styles.container}>
        <Routes />
      </View>
    </Provider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
