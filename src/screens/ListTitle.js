import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {deleteTitle} from '../actions';

class ListTitle extends React.Component {
  render() {
    const {array} = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.navbar}>
          <TouchableOpacity
            onPress={() => {
              Actions.pop();
            }}>
            <Image
              style={styles.popLogo}
              source={require('../images/back_chevron.png')}
            />
          </TouchableOpacity>
          <Text style={styles.navbarText}>Title Lists</Text>
          <View style={styles.tinyLogo} />
        </View>
        <FlatList
          data={array.titleArray}
          renderItem={({item}) => {
            return (
              <View style={styles.list}>
                <Text style={styles.item}>{item.key}</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.props.deleteTitleAction(item.key);
                  }}>
                  <Image
                    style={styles.tinyLogo}
                    source={require('../images/delete.png')}
                  />
                </TouchableOpacity>
              </View>
            );
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#455a64',
    flex: 1,
  },
  navbar: {
    padding: 10,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderColor: '#b0abab',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  navbarText: {
    textAlign: 'center',
    fontSize: 20,
  },
  list: {
    backgroundColor: 'rgba(255, 255,255,0.2)',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginHorizontal: 20,
    marginVertical: 5,
    borderWidth: 1,
    borderColor: '#b0abab',
    borderRadius: 15,
    flexDirection: 'row',
  },
  item: {
    padding: 10,
    fontSize: 18,
    color: '#FFF',
  },
  tinyLogo: {
    width: 25,
    height: 25,
  },
  popLogo: {
    width: 25,
    height: 20,
  },
});

function mapStateToProps(state) {
  return {
    array: state,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    deleteTitleAction: title => dispatch(deleteTitle(title)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ListTitle);
