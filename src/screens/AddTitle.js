import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {setTitle} from '../actions';

class AddTitle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
    };
  }
  render() {
    const {title} = this.state;
    const {array} = this.props;
    const checkTitle = array.titleArray.filter(el => el.key === title);
    return (
      <View style={styles.container}>
        <View style={styles.container}>
          <TextInput
            style={styles.inputBox}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholder="Enter Title"
            placeholderTextColor="#ffffff"
            selectionColor="#fff"
            onChangeText={text => {
              this.setState({
                title: text,
              });
            }}
            value={title}
          />
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              if (title.trim() && title) {
                if (checkTitle.length > 0) {
                  Alert.alert('Title already exist');
                } else {
                  this.props.saveTitleAction(title);
                  Alert.alert('Title added');
                }
              } else {
                Alert.alert('Enter Title');
              }
            }}>
            <Text style={styles.buttonText}>Add Title</Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              Actions.lists();
            }}>
            <Text style={styles.buttonText}>Title List</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#455a64',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputBox: {
    width: 300,
    backgroundColor: 'rgba(255, 255,255,0.2)',
    borderRadius: 25,
    paddingHorizontal: 16,
    fontSize: 16,
    color: '#ffffff',
    marginVertical: 10,
  },
  button: {
    width: 300,
    backgroundColor: '#1c313a',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
  },
});

function mapStateToProps(state) {
  return {
    array: state,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    saveTitleAction: title => dispatch(setTitle(title)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTitle);
