export function setUserEmail(email) {
  return {
    type: 'SET_USER_EMAIL',
    email,
  };
}

export function setUserPassword(password) {
  return {
    type: 'SET_USER_PASSWORD',
    password,
  };
}

export function setUserData(user) {
  return {
    type: 'SET_USER_DATA',
    user,
  };
}

export function setTitle(title) {
  return {
    type: 'SET_TITLE',
    title,
  };
}

export function deleteTitle(title) {
  return {
    type: 'DELETE_TITLE',
    title,
  };
}
