import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  BackHandler,
  TextInput,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  componentDidMount = () => {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  };

  componentWillUnmount = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  };

  onBackPress() {
    BackHandler.exitApp();
  }
  signup = () => {
    Actions.signup();
  };

  loginValidation = () => {
    const {email, password} = this.state;
    const {array} = this.props;
    const checkUser = array.user.filter(el => el.id === email);
    if (checkUser.length > 0) {
      const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      const passwordRegex = /^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
      if (email && emailRegex.test(email)) {
        if (password && passwordRegex.test(password)) {
          if (email) {
            Actions.mainApp();
          } else {
            Alert.alert('Please enter valid credentials to login');
          }
        } else {
          Alert.alert('Invalid password');
        }
      } else {
        Alert.alert('Invalid email', email);
      }
    } else {
      Alert.alert('EmailId does not exist');
    }
  };

  render() {
    const {email, password} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.formContainer}>
          <Image
            style={styles.logo}
            source={require('../images/react_native.png')}
          />
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.inputBox}
              placeholder="Email"
              keyboardType="email-address"
              underlineColorAndroid="transparent"
              onChangeText={emailChange => {
                this.setState({
                  email: emailChange,
                });
              }}
              value={email}
            />
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.inputBox}
              placeholder="Password"
              secureTextEntry={true}
              underlineColorAndroid="transparent"
              onChangeText={passwordChange => {
                this.setState({
                  password: passwordChange,
                });
              }}
              value={password}
            />
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                this.loginValidation();
              }}>
              <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.signupTextCont}>
          <Text style={styles.signupText}>Don't have an account yet?</Text>
          <TouchableOpacity onPress={this.signup}>
            <Text style={styles.signupButton}> Signup</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#455a64',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputBox: {
    width: 300,
    backgroundColor: 'rgba(255, 255,255,0.2)',
    borderRadius: 25,
    paddingHorizontal: 16,
    fontSize: 16,
    color: '#ffffff',
    marginVertical: 10,
  },
  signupTextCont: {
    flexGrow: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingVertical: 16,
    flexDirection: 'row',
  },
  signupText: {
    color: 'rgba(255,255,255,0.6)',
    fontSize: 16,
  },
  signupButton: {
    color: '#ffffff',
    fontSize: 16,
    fontWeight: '500',
  },
  formContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: 300,
    backgroundColor: '#1c313a',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
  },
  logo: {
    width: 100,
    height: 100,
  },
});

function mapStateToProps(state) {
  return {
    array: state,
  };
}

export default connect(mapStateToProps)(Login);
