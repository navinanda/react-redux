import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  BackHandler,
  TextInput,
  Alert,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {setUserData} from '../actions';

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      id: '',
      password: '',
      passwordCopy: '',
    };
  }

  componentDidMount = () => {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  };

  componentWillUnmount = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  };

  onBackPress() {
    Actions.pop();
  }

  goBack() {
    Actions.pop();
  }

  loginValidation = () => {
    const {name, id, password, passwordCopy} = this.state;
    const {array} = this.props;
    const checkUser = array.user.filter(el => el.id === id);
    const user = {
      name,
      id,
      password,
    };
    const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const passwordRegex = /^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
    if (id && emailRegex.test(id)) {
      if (password && passwordRegex.test(password)) {
        if (password === passwordCopy) {
          if (name !== '') {
            if (checkUser.length > 0) {
              Alert.alert('EmailId already registered');
            } else {
              this.props.saveUserDataAction(user);
              Alert.alert('Registeration success. Please login');
              Actions.login();
            }
          } else {
            Alert.alert('Please enter your name');
          }
        } else {
          Alert.alert('Password not matching');
        }
      } else {
        Alert.alert('Invalid password');
      }
    } else {
      Alert.alert('Invalid email');
    }
  };

  render() {
    const {name, id, password, passwordCopy} = this.state;
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.formContainer}>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.inputBox}
              placeholder="Name"
              underlineColorAndroid="transparent"
              onChangeText={nameChange => {
                this.setState({
                  name: nameChange,
                });
              }}
              value={name}
            />
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.inputBox}
              placeholder="Email"
              keyboardType="email-address"
              underlineColorAndroid="transparent"
              onChangeText={emailChange => {
                this.setState({
                  id: emailChange,
                });
              }}
              value={id}
            />
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.inputBox}
              placeholder="Password"
              secureTextEntry={true}
              underlineColorAndroid="transparent"
              onChangeText={passwordChange => {
                this.setState({
                  password: passwordChange,
                });
              }}
              value={password}
            />
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.inputBox}
              placeholder="Confirm password"
              secureTextEntry={true}
              underlineColorAndroid="transparent"
              onChangeText={passwordChange => {
                this.setState({
                  passwordCopy: passwordChange,
                });
              }}
              value={passwordCopy}
            />
          </View>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              this.loginValidation();
            }}>
            <Text style={styles.buttonText}>Register</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.signupTextCont}>
          <Text style={styles.signupText}>Already have an account?</Text>
          <TouchableOpacity onPress={this.goBack}>
            <Text style={styles.signupButton}> Sign in</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#455a64',
    flexGrow: 1,
    paddingVertical: 30,
  },
  inputBox: {
    width: 300,
    backgroundColor: 'rgba(255, 255,255,0.2)',
    borderRadius: 25,
    paddingHorizontal: 16,
    fontSize: 16,
    color: '#ffffff',
    marginVertical: 10,
  },
  signupTextCont: {
    flexGrow: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingVertical: 16,
    flexDirection: 'row',
  },
  signupText: {
    color: 'rgba(255,255,255,0.6)',
    fontSize: 16,
  },
  signupButton: {
    color: '#ffffff',
    fontSize: 16,
    fontWeight: '500',
  },
  formContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: 300,
    backgroundColor: '#1c313a',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
  },
});

function mapStateToProps(state) {
  return {
    array: state,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    saveUserDataAction: user => dispatch(setUserData(user)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
