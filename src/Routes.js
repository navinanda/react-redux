import React from 'react';
import {Router, Stack, Scene} from 'react-native-router-flux';
import Login from './login/Login';
import Signup from './login/Signup';
import AddTitle from './screens/AddTitle';
import ListTitle from './screens/ListTitle';

class Routes extends React.Component {
  render() {
    return (
      <Router>
        <Stack key="root" hideNavBar={true}>
          <Scene key="login" component={Login} title="Login" initial={true} />
          <Scene key="signup" component={Signup} title="Register" />
          <Stack key="mainApp" hideNavBar={true}>
            <Scene key="addtitle" component={AddTitle} title="AddTitle" />
            <Scene key="lists" component={ListTitle} title="AddTitle" />
          </Stack>
        </Stack>
      </Router>
    );
  }
}

export default Routes;
